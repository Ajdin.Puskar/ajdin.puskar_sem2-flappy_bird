package inf101v22.flappybird.Handlers;

import static org.junit.jupiter.api.Assertions.*;

import inf101v22.flappybird.Aspects.FlappyBird;
import inf101v22.flappybird.Aspects.FlappyTube;
import inf101v22.flappybird.Supers.FlappyObject;
import java.awt.Graphics;
import org.junit.jupiter.api.Test;

class FlappyObjectHandlerTest {

  FlappyObjectHandler objectHandler;
  FlappyObject object = new FlappyObject(50, 50, 51, 36) {
    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics graphics) {

    }
  };

  @Test
  void addObject() {
    if (object != null) {
      if (object instanceof FlappyBird && this.object.getBounds().intersects(object.getBounds())) {
        assertTrue(true);
      }
    }
  }

  @Test
  void removeObject() {
    if (object == null) {
      if (object instanceof FlappyTube && this.object.getBounds().intersects(object.getBounds())) {
        assertTrue(true);
      }
    }
  }
}