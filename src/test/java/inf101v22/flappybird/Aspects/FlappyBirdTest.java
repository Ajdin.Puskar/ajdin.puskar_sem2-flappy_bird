package inf101v22.flappybird.Aspects;

import static inf101v22.flappybird.Mains.FlappyGame.gameover;
import static org.junit.jupiter.api.Assertions.*;

import inf101v22.flappybird.Handlers.FlappyObjectHandler;
import inf101v22.flappybird.Supers.FlappyAnimation;
import inf101v22.flappybird.Supers.FlappyObject;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import org.junit.jupiter.api.Test;

class FlappyBirdTest {

  FlappyObject object = new FlappyObject(50, 50, 51, 36) {
    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics graphics) {

    }
  };
  FlappyObjectHandler objectHandler;
  FlappyAnimation animation;

  @Test
  void tick() {
    objectHandler = new FlappyObjectHandler();
    if (object == null) {
      if (object instanceof FlappyTube && this.object.getBounds().intersects(object.getBounds())) {
        assertFalse(true);
      }
    }
  }

  @Test
  void render() {
    BufferedImage[] images = new BufferedImage[3];

    animation = new FlappyAnimation(object,100, true, images);

    if (animation.isLoop()) {
      assertTrue(true);
    }
  }
}
