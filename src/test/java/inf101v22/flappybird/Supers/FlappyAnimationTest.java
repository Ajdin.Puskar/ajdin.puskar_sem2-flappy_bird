package inf101v22.flappybird.Supers;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FlappyAnimationTest {

  public boolean running;
  public boolean loop;
  public int currentImage;
  public long startTime;

  @Test
  void start() {
    if (running) {
      currentImage = 0;
      startTime = 0L;
      assertTrue(true);
    }
  }

  @Test
  void stop() {
    if (!running) {
      currentImage = 0;
      startTime = 0L;
      assertTrue(true);
    }
  }

  @Test
  void isLoop() {
    if (loop) {
      assertTrue(true);
    }

  }

  @Test
  void isRunning() {
    if (running) {
      assertTrue(true);
    }
  }
}