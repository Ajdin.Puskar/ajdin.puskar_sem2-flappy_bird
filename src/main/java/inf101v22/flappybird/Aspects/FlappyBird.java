package inf101v22.flappybird.Aspects;

import static inf101v22.flappybird.Mains.FlappyGame.gameover;

import inf101v22.flappybird.Handlers.FlappyObjectHandler;
import inf101v22.flappybird.Loaders.GraphicsLoader;
import inf101v22.flappybird.Mains.FlappyGame;
import inf101v22.flappybird.Supers.FlappyAnimation;
import inf101v22.flappybird.Supers.FlappyObject;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class FlappyBird extends FlappyObject {

	/*
	 * This class represents the classic bird of the game which you can control with
	 * the spacebar.
	 */

	// field variables
	FlappyAnimation flappyAnimation;

	// field variables
	public float gravity;
	public float speed;

	/**
	 * constructor to draw the bird, animate it with the animation method, and start
	 * the animation
	 *
	 * @param x      value of the bird
	 * @param y      value of the bird
	 * @param width  of the bird
	 * @param height of the bird
	 * @throws IOException, if the images of the bird is nowhere to be found
	 */
	public FlappyBird(int x, int y, int width, int height) throws IOException {
		super(x, y, width, height);

		gravity = 0.3f;
		speed = 12f;

		// buffering through three bird images
		BufferedImage[] images = new BufferedImage[3];

		// loading the three images with my GraphicsLoader class
		for (int i = 0; i < images.length; i++) {
			images[i] = GraphicsLoader.loadGraphics("bird" + i + ".png");
		}

		// animating the bird
		flappyAnimation = new FlappyAnimation(this, 100, true, images);
		flappyAnimation.start();

		FlappyObjectHandler.addObject(this);
	}

	/**
	 * Method to calculate gravity & falling speed
	 */
	@Override
	public void tick() {
		velocityY += gravity;
		y += velocityY;

		if (velocityY > speed) {
			velocityY = speed;
		}

		if (y + height > FlappyGame.HEIGHT - 168) {
			y = FlappyGame.HEIGHT - 166 - height;
			setVelocityY(0);
		}

		if (y < 0) {
			y = 0;
			setVelocityY(0);
		}

		// game over if the bird hits a tube
		FlappyObject temp = null;
		for (int i = 0; i < FlappyObjectHandler.linkedList.size(); ++i) {
			temp = FlappyObjectHandler.linkedList.get(i);
			if (temp instanceof FlappyTube && this.getBounds().intersects(temp.getBounds())) {
				gameover = true;
			}
		}

		this.flappyAnimation.tick();
	}

	// method to render the animation

	/**
	 * method to render the animation
	 *
	 * @param graphics launches graphics towards the animation render
	 */
	@Override
	public void render(Graphics graphics) {

		flappyAnimation.render(graphics);
	}
}
