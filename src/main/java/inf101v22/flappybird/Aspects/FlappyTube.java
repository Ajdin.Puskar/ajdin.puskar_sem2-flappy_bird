package inf101v22.flappybird.Aspects;

import inf101v22.flappybird.Enumerators.FlappyTubeType;
import inf101v22.flappybird.Handlers.FlappyObjectHandler;
import inf101v22.flappybird.Loaders.GraphicsLoader;
import inf101v22.flappybird.Mains.FlappyGame;
import inf101v22.flappybird.Supers.FlappyObject;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class FlappyTube extends FlappyObject {

	/*
	 * This class represents the classic tubes used in Flappy Bird, which the bird
	 * flies through.
	 */

	// field variables
	FlappyTubeType type;
	BufferedImage tubeBlock;
	BufferedImage tube;

	/**
	 * constructor to create the tube which the bird passes through
	 *
	 * @param x      sets the x value of the tube
	 * @param y      sets the y value of the tube
	 * @param width  sets the width of the tube
	 * @param height sets the height of the tube
	 * @param type   sets which type of tube we are using, as we have the initial
	 *               tube, top and bottom
	 * @throws IOException when images of the tubes are nowhere to be found
	 */
	public FlappyTube(int x, int y, int width, int height, FlappyTubeType type) throws IOException {
		super(x, y, width, height);

		this.type = type;
		this.velocityX = 3;

		// loading the tubes with my GraphicsLoader class
		tube = GraphicsLoader.loadGraphics("tube.png");

		// loading the tubes edges with my GraphicsLoader class
		if (type == FlappyTubeType.BOTTOM) {
			tubeBlock = GraphicsLoader.loadGraphics("tubebottomdown.png");
		} else if (type == FlappyTubeType.TOP) {
			tubeBlock = GraphicsLoader.loadGraphics("tubebottomtop.png");
		}
	}

	@Override
	public void tick() {
		x -= velocityX;

		if (x + width < 0) {
			FlappyObjectHandler.removeObject(this);

			/*
			 * Score counter as the bird passes through the tube openings. It originally
			 * counted double as it passes through two sides of the tubes, but I corrected
			 * it to count only towards the top-tube.
			 */
			if (type == FlappyTubeType.TOP) {
				FlappyGame.score += 1;
			}
		}
	}

	/**
	 *
	 * @param graphics renders the tubes
	 */
	@Override
	public void render(Graphics graphics) {
		if (type == FlappyTubeType.BOTTOM) {
			graphics.drawImage(tube, x, y, 72, height, null);
			graphics.drawImage(tubeBlock, x - 3, y, null);
		} else if (type == FlappyTubeType.TOP) {
			graphics.drawImage(tube, x, y, 72, height, null);
			graphics.drawImage(tubeBlock, x - 3, y + height - 36, null);
		}
	}
}
