package inf101v22.flappybird.Aspects;

import inf101v22.flappybird.Loaders.GraphicsLoader;
import inf101v22.flappybird.Mains.FlappyGame;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class FlappyGround {

	/*
	 * This class represents the ground/plane of the game which the bird cannot fall
	 * through, inspired by the ground from the original game.
	 */

	// field variables
	private BufferedImage image;

	// field variables
	private int x1;
	private int x2;

	// field variables
	private float velocityX;

	/**
	 * constructor of the ground part of the background
	 *
	 * @throws IOException if the ground image is nowhere to be found
	 */
	public FlappyGround() throws IOException {
		x1 = 0;
		x2 = FlappyGame.WIDTH;

		velocityX = 3;

		// loading the image of the ground with my GraphicsLoader class
		image = GraphicsLoader.loadGraphics("ground.png");
	}

	public void tick() {
		x1 -= velocityX;
		x2 -= velocityX;

		if (x1 + FlappyGame.WIDTH < 0) {
			x1 = FlappyGame.WIDTH;
		}

		if (x2 + FlappyGame.WIDTH < 0) {
			x2 = FlappyGame.WIDTH;
		}
	}

	/**
	 *
	 * @param graphics renders the ground
	 */
	public void render(Graphics graphics) {
		graphics.drawImage(image, x1, FlappyGame.HEIGHT - 168, null);
		graphics.drawImage(image, x2, FlappyGame.HEIGHT - 168, null);
	}

}
