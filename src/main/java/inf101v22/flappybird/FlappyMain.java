package inf101v22.flappybird;

import inf101v22.flappybird.Mains.FlappyGame;
import inf101v22.flappybird.Mains.FlappyWindow;
import java.io.IOException;

public class FlappyMain {

	public static void main(String[] args) throws IOException {
		new FlappyWindow(FlappyGame.WIDTH, FlappyGame.HEIGHT, "INF101 FlappyBird", new FlappyGame());
	}

}
