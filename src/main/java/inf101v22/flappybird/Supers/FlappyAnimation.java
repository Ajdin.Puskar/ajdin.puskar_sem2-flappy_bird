package inf101v22.flappybird.Supers;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class FlappyAnimation implements AnimationInterface {

	/*
	 * This class represents the animation of the bird, which is being run through a
	 * loop, but shuts down at game over.
	 */

	// field variables
	private int x;
	private int y;
	private int currentImage;

	// field variables
	private long delay;
	private long startTime;

	// field variables
	private boolean loop;
	private boolean running;

	// field variables
	private FlappyObject object;
	private BufferedImage[] images;

	/**
	 * constructor to construct an object to animate
	 *
	 * @param object chooses which object to use
	 * @param delay  sets time between animation switches
	 * @param loop   loops the animation
	 * @param images chooses which images to use
	 */
	public FlappyAnimation(FlappyObject object, long delay, boolean loop, BufferedImage[] images) {
		this.x = object.getX();
		this.y = object.getY();
		this.currentImage = 0;
		this.delay = delay;
		this.startTime = 0L;
		this.loop = loop;
		this.setObject(object);
		this.images = images;
	}

	/**
	 * Method to render a specific animation In this case it renders the animation
	 * of the bird The bird-render contains three images which sort of forms a gif
	 * animation
	 *
	 * @param g, initialized Graphics
	 */
	public void render(Graphics g) {
		if (this.object == null) {
			g.drawImage(this.images[this.currentImage], this.x, this.y, null);
		} else {
			g.drawImage(this.images[this.currentImage], object.x, object.y, null);
		}

	}

	/**
	 * Method to calculate ticks between image swaps In this case ticks between
	 * image switches Which forms the gif
	 *
	 */
	public void tick() {
		long pastTime = (System.nanoTime() - this.startTime) / 1000000L;
		if (pastTime >= this.delay && this.running) {
			++this.currentImage;
			this.startTime = System.nanoTime();
		}

		if (this.currentImage == this.images.length) {
			this.currentImage = 0;
			if (!this.loop) {
				this.stop();
			}
		}

	}

	/**
	 * Method to start the ticks between image swaps Making the gif take form
	 */
	public void start() {
		this.running = true;
		this.currentImage = 0;
		this.startTime = 0L;
	}

	/**
	 * Method to stop the gif when the game stops running
	 */
	public void stop() {
		this.running = false;
		this.currentImage = 0;
		this.startTime = 0L;
	}

	/*
	 * AUTO-generated setters and getters
	 *
	 * All of these has descriptions inside the Interface
	 */
	public FlappyObject getObject() {
		return this.object;
	}

	public void setObject(FlappyObject object) {
		this.object = object;
	}

	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getCurrentImage() {
		return this.currentImage;
	}

	public void setCurrentImage(int currentImage) {
		this.currentImage = currentImage;
	}

	public long getDelay() {
		return this.delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public boolean isLoop() {
		return this.loop;
	}

	public void getLoop(boolean loop) {
		this.loop = loop;
	}

	public boolean isRunning() {
		return this.running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public BufferedImage[] getImages() {
		return this.images;
	}

	public void setImages(BufferedImage[] images) {
		this.images = images;
	}
}
