package inf101v22.flappybird.Supers;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class FlappyButton {

	/*
	 * This class represents the button which shows when the bird has fallen/died.
	 */

	// field variables
	public int x;
	public int y;
	public int width;
	public int height;

	// field variables
	public static boolean pressed;

	// field variables
	private BufferedImage image;

	/**
	 * constructor to construct a button, in this case the 'Play Again' button
	 *
	 * @param x      sets the x value of the button
	 * @param y      sets the y value of the button
	 * @param width  sets the width of the button
	 * @param height sets the height of the button
	 * @param image  chooses which image to use for the button
	 */
	public FlappyButton(int x, int y, int width, int height, BufferedImage image) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.image = image;
	}

	/**
	 * a collision method to check if the mouse cursor is in range of the button
	 *
	 * @param mouseX sets the x values the mouse cursor has to be in range for the
	 *               button to work
	 * @param mouseY sets the y value the mouse cursor has to be in range for the
	 *               button to work
	 * @param button signs the computer that the image displayed is indeed a button
	 * @return boolean that signs the computer that the button is clickable
	 */
	public static boolean checkCollision(int mouseX, int mouseY, FlappyButton button) {
		return mouseX >= button.x && mouseX <= button.x + button.width && mouseY >= button.y
				&& mouseY <= button.y + button.height;
	}

	/**
	 *
	 * @param graphics renders the button image
	 */
	public void render(Graphics graphics) {
		if (pressed) {
			graphics.drawImage(image, x + 1, y + 1, width - 2, height - 2, null);
		} else {
			graphics.drawImage(image, x, y, null);
		}
	}

}
