package inf101v22.flappybird.Supers;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public interface AnimationInterface {

	/**
	 * Method to render a specific animation In this case it renders the animation
	 * of the bird The bird-render contains three images which sort of forms a gif
	 * animation
	 *
	 * @param g, initialized Graphics
	 */
	void render(Graphics g);

	/**
	 * Method to calculate ticks between image swaps In this case ticks between
	 * image switches Which forms the gif
	 *
	 */
	void tick();

	/**
	 * Method to start the ticks between image swaps Making the gif take form
	 */
	void start();

	/**
	 * Method to stop the gif when the game stops running
	 */
	void stop();

	/**
	 * gets a specific object
	 *
	 * @return object
	 */
	FlappyObject getObject();

	/**
	 *
	 * @param object, sets an object in the scope
	 */
	void setObject(FlappyObject object);

	/**
	 * gets x value
	 *
	 * @return x
	 */
	int getX();

	/**
	 *
	 * @param x, sets x value
	 *
	 */
	void setX(int x);

	/**
	 * gets y value
	 *
	 * @return y
	 */
	int getY();

	/**
	 *
	 * @param y, sets y value
	 */
	void setY(int y);

	/**
	 * gets the current image that is being displayed
	 *
	 * @return currentImage
	 */
	int getCurrentImage();

	/**
	 *
	 * @param currentImage, sets an image to be displayed
	 */
	void setCurrentImage(int currentImage);

	/**
	 * gets an integer (64-bit integer)
	 *
	 * @return delay
	 */
	long getDelay();

	/**
	 *
	 * @param delay, sets an integer (64-bit integer)
	 */
	void setDelay(long delay);

	/**
	 * boolean to activate or deactivate a loop
	 *
	 * @return loop
	 */
	boolean isLoop();

	/**
	 *
	 * @param loop, gets a loop
	 */
	void getLoop(boolean loop);

	/**
	 * boolean to identify if the game is running or not
	 *
	 * @return running
	 */
	boolean isRunning();

	/**
	 *
	 * @param running, activates or deactivates the game
	 */
	void setRunning(boolean running);

	/**
	 * gets images, in this case buffering between the three bird images
	 *
	 * @return images
	 */
	BufferedImage[] getImages();

	/**
	 *
	 * @param images, sets which images to buffer through
	 */
	void setImages(BufferedImage[] images);
}
