package inf101v22.flappybird.Supers;

import java.awt.Graphics;
import java.awt.Rectangle;

public abstract class FlappyObject implements ObjectInterface {

	/*
	 * This class represents generic objects in the game, and is also an abstract
	 * class, which means the class cannot be instantiated, but can be subclassed.
	 */

	/*
	 * Protected field variables, which means they can be accessed by any class in
	 * the same package.
	 */
	protected int x;
	protected int y;
	protected int width;
	protected int height;

	// protected field variables
	protected float velocityX;
	protected float velocityY;

	/**
	 * constructor to construct a given object
	 *
	 * @param x      sets the x value of an object
	 * @param y      sets the y value of an object
	 * @param width  sets the width of an object
	 * @param height sets the height of an object
	 */
	public FlappyObject(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	// abstract methods
	public abstract void tick();

	public abstract void render(Graphics graphics);

	// method to get bounds of rectangle
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}

	/*
	 * AUTO-generated setters and getters
	 *
	 * Descriptions of the setters and getters are found in the Interface
	 */
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public float getVelocityX() {
		return velocityX;
	}

	public void setVelocityX(float velocityX) {
		this.velocityX = velocityX;
	}

	public float getVelocityY() {
		return velocityY;
	}

	public void setVelocityY(float velocityY) {
		this.velocityY = velocityY;
	}
}
