package inf101v22.flappybird.Supers;

public interface ObjectInterface {

	/**
	 * gets x value
	 *
	 * @return x
	 */
	int getX();

	/**
	 *
	 * @param x, sets x value
	 *
	 */
	void setX(int x);

	/**
	 * gets y value
	 *
	 * @return y
	 */
	int getY();

	/**
	 *
	 * @param y, sets y value
	 */
	void setY(int y);

	/**
	 * gets value of width
	 *
	 * @return width
	 */
	int getWidth();

	/**
	 *
	 * @param width, sets value of width
	 */
	void setWidth(int width);

	/**
	 * gets value of height
	 *
	 * @return height
	 */
	int getHeight();

	/**
	 *
	 * @param height, sets value of height
	 */
	void setHeight(int height);

	/**
	 * gets value of velocity in x-direction
	 *
	 * @return velocityX
	 */
	float getVelocityX();

	/**
	 *
	 * @param velocityX, sets value of velocity in x-direction
	 */
	void setVelocityX(float velocityX);

	/**
	 * gets value of velocity in y-direction
	 *
	 * @return velocityY
	 */
	float getVelocityY();

	/**
	 *
	 * @param velocityY, sets value of velocity in y-direction
	 */
	void setVelocityY(float velocityY);

}
