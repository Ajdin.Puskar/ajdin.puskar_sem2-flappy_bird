package inf101v22.flappybird.Enumerators;

public enum FlappyTubeType {
	/**
	 * Indicates which type of tube we are using.
	 *
	 * The initial tube is not on the list as we only needed to declare the top and
	 * bottom for the game to work properly
	 */
	BOTTOM, TOP;
}
