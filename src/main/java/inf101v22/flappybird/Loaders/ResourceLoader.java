package inf101v22.flappybird.Loaders;

import java.io.InputStream;

public class ResourceLoader {

	/**
	 *
	 * @param path aims towards the file path of your wanted resource
	 * @return an inputStream (estimate number of bytes that can be read)
	 */

	public static InputStream load(String path) {
		InputStream inputStream = ResourceLoader.class.getResourceAsStream(path);

		if (inputStream == null) {
			inputStream = ResourceLoader.class.getResourceAsStream("/" + path);
		}

		return inputStream;
	}

}
