package inf101v22.flappybird.Loaders;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

public class GraphicsLoader {

	/**
	 *
	 * @param path aims towards the file path of your wanted graphics
	 * @return the image you have written in your path
	 * @throws IOException when the image is nowhere to be found
	 */
	public static BufferedImage loadGraphics(String path) throws IOException {
		BufferedImage image = null;

		image = ImageIO.read(ResourceLoader.load("/" + path));

		return image;
	}

}
