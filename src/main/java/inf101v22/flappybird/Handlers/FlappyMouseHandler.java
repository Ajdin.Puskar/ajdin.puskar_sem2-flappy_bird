package inf101v22.flappybird.Handlers;

import inf101v22.flappybird.Mains.FlappyGame;
import inf101v22.flappybird.Supers.FlappyButton;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FlappyMouseHandler implements MouseListener {

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	/**
	 *
	 * @param e registers the button being clicked, which triggers the game to
	 *          restart. It will only appear when boolean gameover = true.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if (FlappyButton.checkCollision(e.getX(), e.getY(), FlappyGame.startButton)) {
			if (FlappyGame.gameover) {
				FlappyButton.pressed = true;
				FlappyObjectHandler.linkedList.clear();
				FlappyObjectHandler.addObject(FlappyGame.flappyBird);
				FlappyGame.gameover = false;
				FlappyGame.score = 0;
				FlappyButton.pressed = false;
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}
}
