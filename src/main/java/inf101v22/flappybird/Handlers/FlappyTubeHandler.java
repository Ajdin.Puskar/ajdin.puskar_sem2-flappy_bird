package inf101v22.flappybird.Handlers;

import inf101v22.flappybird.Enumerators.FlappyTubeType;
import inf101v22.flappybird.Aspects.FlappyTube;
import inf101v22.flappybird.Mains.FlappyGame;
import java.io.IOException;
import java.util.Random;

public class FlappyTubeHandler {

	/*
	 * This class represents the behavior of the tubes, how they are placed, spaced,
	 * and spawned.
	 */

	private static Random rd = new Random();

	// field variables
	public static int groundSize = 168;
	public static int area = FlappyGame.HEIGHT - groundSize;
	public static int spacing = 120;
	public static int minSize = 40;
	public static int maxSize = area - spacing - minSize;
	public static int delay = 1;
	public static int now;

	/**
	 * static method to spawn tubes, including top and bottom parts of the tubes
	 *
	 * @throws IOException when images are nowhere to be found
	 */
	public static void spawnTube() throws IOException {
		int heightTop = rd.nextInt(maxSize) + 1;

		while (heightTop < minSize) {
			heightTop = rd.nextInt(maxSize) + 1;
		}

		int heightBottom = area - spacing - heightTop;

		FlappyTube tubeTop = new FlappyTube(500, 0, 78, heightTop, FlappyTubeType.TOP);
		FlappyTube tubeBottom = new FlappyTube(500, heightTop + spacing, 78, heightBottom, FlappyTubeType.BOTTOM);

		FlappyObjectHandler.addObject(tubeTop);
		FlappyObjectHandler.addObject(tubeBottom);
	}

	public static void tick() throws IOException {
		if (now < delay) {
			now++;
		} else {
			spawnTube();
			now = 0;
		}
	}

}
