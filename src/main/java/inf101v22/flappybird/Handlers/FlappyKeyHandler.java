package inf101v22.flappybird.Handlers;

import inf101v22.flappybird.Mains.FlappyGame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class FlappyKeyHandler implements KeyListener {

	public FlappyKeyHandler() {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	/**
	 *
	 * @param e registers the key being pressed, in this case SPACEBAR
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			FlappyGame.flappyBird.setVelocityY(-5.0F);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}
}
