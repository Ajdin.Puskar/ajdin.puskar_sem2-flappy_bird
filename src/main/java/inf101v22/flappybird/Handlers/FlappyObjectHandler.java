package inf101v22.flappybird.Handlers;

import inf101v22.flappybird.Supers.FlappyObject;
import java.awt.Graphics;
import java.util.LinkedList;

public class FlappyObjectHandler {

	public static LinkedList<FlappyObject> linkedList = new LinkedList<FlappyObject>();

	/**
	 *
	 * @param object, in this case adds an object. Reused lots to add tubes,
	 *                top-tubes and bottom-tubes
	 */
	public static void addObject(FlappyObject object) {
		linkedList.add(object);
	}

	/**
	 *
	 * @param object, in this case removes an object. Again reused lots to remove
	 *                tube-types after it has passed the screen
	 */
	public static void removeObject(FlappyObject object) {
		linkedList.remove(object);
	}

	/**
	 *
	 * @param graphics, temporary render
	 */
	public static void render(Graphics graphics) {
		FlappyObject temp = null;

		for (int i = 0; i < linkedList.size(); i++) {
			temp = linkedList.get(i);
			temp.render(graphics);
		}
	}

	/**
	 * temporary tick
	 */
	public static void tick() {
		FlappyObject temp = null;

		for (int i = 0; i < linkedList.size(); i++) {
			temp = linkedList.get(i);
			temp.tick();
		}
	}

}
