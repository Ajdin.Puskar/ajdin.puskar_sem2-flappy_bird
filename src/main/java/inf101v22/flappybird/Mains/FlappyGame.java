package inf101v22.flappybird.Mains;

import inf101v22.flappybird.Aspects.FlappyBird;
import inf101v22.flappybird.Aspects.FlappyGround;
import inf101v22.flappybird.Handlers.FlappyKeyHandler;
import inf101v22.flappybird.Handlers.FlappyMouseHandler;
import inf101v22.flappybird.Handlers.FlappyObjectHandler;
import inf101v22.flappybird.Handlers.FlappyTubeHandler;
import inf101v22.flappybird.Loaders.GraphicsLoader;
import inf101v22.flappybird.Song.FlappySong;
import inf101v22.flappybird.Supers.FlappyButton;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class FlappyGame extends Canvas implements Runnable, GameInterface {

	/*
	 * This class represents almost the whole game, as we have seen with in Tetris,
	 * where TetrisModel, and TetrisBoard represented almost the entirety of the
	 * Tetris game.
	 */

	// field variables
	public static final int WIDTH = 432;
	public static final int HEIGHT = 768;
	public static int score;

	// field variables
	public boolean running;
	public static boolean gameover;

	// field variables
	public static FlappyBird flappyBird;
	public static BufferedImage background;
	public static BufferedImage img_gameover;
	public static FlappyGround ground;
	public static FlappyButton startButton;

	// field variables
	Thread thread;

	/**
	 * Method to start all aspects of the game synchronized
	 */
	public synchronized void start() {
		running = true;
		thread = new Thread();
		thread.start();
		run();

	}

	/**
	 * Method to initialize KeyListener, MouseListener and different other object in
	 * the game, such as images like background etc.
	 *
	 * @throws IOException, when loading images if the images is nowhere to be found
	 */
	public void init() throws IOException {

		addKeyListener(new FlappyKeyHandler());
		addMouseListener(new FlappyMouseHandler());

		img_gameover = GraphicsLoader.loadGraphics("gameover.png");
		background = GraphicsLoader.loadGraphics("background.png");
		ground = new FlappyGround();
		flappyBird = new FlappyBird(50, 50, 51, 36);
		startButton = new FlappyButton(138, 200, 156, 87, GraphicsLoader.loadGraphics("playbutton.png"));
		new FlappySong().run();

	}

	/**
	 * Method on how to handle objects when gameover = true
	 */
	public void tick() {
		if (!gameover) {
			FlappyObjectHandler.tick();
			ground.tick();
		}
	}

	/**
	 * Method to render pieces of graphics
	 */
	public void render() {
		BufferStrategy bufferStrategy = this.getBufferStrategy();

		if (bufferStrategy == null) {
			createBufferStrategy(3);
			return;
		}

		Graphics graphics = bufferStrategy.getDrawGraphics();

		graphics.drawImage(background, 0, 0, null);
		ground.render(graphics);

		FlappyObjectHandler.render(graphics);

		if (gameover) {
			graphics.drawImage(img_gameover, WIDTH / 2 - 288 / 2, 50, null);
			startButton.render(graphics);
		}

		graphics.setColor(Color.ORANGE);
		graphics.setFont(new Font("Cascadia Mono", Font.BOLD, 20));
		graphics.drawString("" + score, 5, 20);

		graphics.setColor(Color.ORANGE);
		graphics.setFont(new Font("Cascadia Mono", Font.BOLD, 20));
		graphics.drawString("Controls", 5, HEIGHT - 70);

		graphics.setColor(Color.ORANGE);
		graphics.setFont(new Font("Cascadia Mono", Font.BOLD, 20));
		graphics.drawString("SPACEBAR: JUMP", 5, HEIGHT - 50);

		graphics.setColor(Color.ORANGE);
		graphics.setFont(new Font("Cascadia Mono", Font.BOLD, 20));
		graphics.drawString("CICK PLAY BUTTON: RESTART", 5, HEIGHT - 30);

		graphics.dispose();
		bufferStrategy.show();

	}

	/**
	 * Method to run the initializer Provides FPS and TICK counter
	 *
	 * Has an AUTO-generated IOException installed
	 */
	@Override
	public void run() {
		try {
			init();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.requestFocus();

		long pastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		int updates = 0;
		int frames = 0;

		while (running) {
			long now = System.nanoTime();
			delta += (now - pastTime) / ns;
			pastTime = now;

			while (delta > 0) {
				tick();
				updates++;

				render();
				frames++;

				delta--;
			}

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				System.out.println("FPS: " + frames + " | TICKS: " + updates);

				try {
					FlappyTubeHandler.tick();
				} catch (IOException e) {
					e.printStackTrace();
				}

				updates = 0;
				frames = 0;

			}
		}

	}
}
