package inf101v22.flappybird.Mains;

import java.io.IOException;
import javax.swing.JFrame;

public class FlappyWindow extends JFrame {

	public FlappyWindow(int width, int height, String title, FlappyGame main) throws IOException {

		/*
		 * This class represents the game window, whereas I copied most of it from
		 * TetrisMain
		 */

		// sets title
		setTitle(title);

		// sizes the frame
		pack();

		// sets preferred frame size
		setSize(width + getInsets().left + getInsets().right, height + getInsets().top + getInsets().bottom);

		// center the gui to the screen
		setLocationRelativeTo(null);

		// prevents the game window to be resized
		setResizable(false);

		// sets default close operation
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// displays the components
		setVisible(true);

		// adds FlappyGame
		add(main);

		// starts FlappyGame, which is launched through FlappyMain
		main.start();
	}

}
