package inf101v22.flappybird.Mains;

import java.io.IOException;

public interface GameInterface {

	/**
	 * Method to initialize KeyListener, MouseListener and different other object in
	 * the game, such as images like background etc.
	 *
	 * @throws IOException, when loading images if the images is nowhere to be found
	 */
	void init() throws IOException;

	/**
	 * Method to start all aspects of the game synchronized
	 */
	void start();

	/**
	 * Method on how to handle objects when gameover = true
	 */
	void tick();

	/**
	 * Method to render pieces of graphics
	 */
	void render();

	/**
	 * Method to run the initializer Provides FPS and TICK counter
	 *
	 * Has an AUTO-generated IOException installed
	 */
	void run();
}
