# FLAPPY BIRD 


<- NORSK ->

## INSTRUKSJONER

Hei <bruker>! Jeg har laget en kopi av det klassiske Flappy Bird spillet som gikk
viralt for en del år tilbake. Spillet er blant det enkleste du noen gang kommer til
å spille, men kan også være noe av det vanskeligere du har spilt, når det kommer 
til å få en bra poengsum.

Hvordan kontrollere Flappy Bird:

1. Du bruker kun MELLOMROM/SPACE knappen for å kontrollere Flappy Bird.
2. Når fuglen har falt/dødd er det bare å trykke på PLAY knappen som dukker opp på skjermen for å starte på nytt.

Demonstrerende video:

https://vimeo.com/704060418

Enkle kontroller, lett å spille.


<- ENGLISH ->

## INSTRUCTIONS

Hello <user>! I've made a copy of the all-time classic game, Flappy Bird, which went
viral a couple of years ago. The game is among the easiest you will ever play, 
but can also be one of the hardest ones, when it comes to your highscore.

How to control the Flappy Bird:

1. To control Flappy Bird, you only need to use the SPACE-bar.
2. When Flappy Bird has fallen/died, you only need to press the PLAY button which pops up on your screen to restart.

Demonstration:

https://vimeo.com/704060418

Simple controls, easy to play.

### GJENBRUK AV KODE

I programmet finner du ekstremt mye gjenbruk av gode, slik som metodene tick() og render(), 
og du finner mye bruk av hjelpe klassen GraphicsLoader som brukes hver gang noe skal rendere og bli 
vist på skjermen. GraphicsLoader er en klasse som laster inn bilder fra Resource mappen som ligger 
sammen med main.java mappen.

#### TESTER

I programmet finner du ikke mange tester da mye av koden er veldig generisk og det er brukt mye 
gjenbruk av kode. Da jeg bruker ALT+INSERT knappen på tastaturet får jeg opp hvilke tester jeg kan lage
til de forskjellige klassene, og da valgte jeg å bare opprette en test for tick() og render(), da de blir brukt
i så og si hver eneste klasse.

Ellers skal det være fire tester til programmet, som tester metoder, hjelpemetoder og hjelpeklassen GraphicsLoader.