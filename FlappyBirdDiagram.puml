@startuml

title FLAPPY BIRD CLASS DIAGRAM

namespace inf101v22.flappybird {
    namespace Aspects {
      class inf101v22.flappybird.Aspects.FlappyBird {
          + gravity : float
          + speed : float
          + FlappyBird()
          + render()
          + tick()
      }
    }
  }

namespace inf101v22.flappybird {
    namespace Aspects {
      class inf101v22.flappybird.Aspects.FlappyGround {
          - image : BufferedImage
          - velocityX : float
          - x1 : int
          - x2 : int
          + FlappyGround()
          + render()
          + tick()
      }
    }
  }


  namespace inf101v22.flappybird {
    namespace Aspects {
      class inf101v22.flappybird.Aspects.FlappyTube {
          ~ tube : BufferedImage
          ~ tubeBlock : BufferedImage
          + FlappyTube()
          + render()
          + tick()
      }
    }
  }


  inf101v22.flappybird.Aspects.FlappyBird -up-|> inf101v22.flappybird.Supers.FlappyObject
  inf101v22.flappybird.Aspects.FlappyBird o-- inf101v22.flappybird.Supers.FlappyAnimation : flappyAnimation
  inf101v22.flappybird.Aspects.FlappyTube -up-|> inf101v22.flappybird.Supers.FlappyObject
  inf101v22.flappybird.Aspects.FlappyTube o-- inf101v22.flappybird.Enumerators.FlappyTubeType : type

  namespace inf101v22.flappybird {
      namespace Enumerators {
        enum FlappyTubeType {
          BOTTOM
          TOP
        }
      }
    }

  namespace inf101v22.flappybird {
      namespace Handlers {
        class inf101v22.flappybird.Handlers.FlappyKeyHandler {
            + FlappyKeyHandler()
            + keyPressed()
            + keyReleased()
            + keyTyped()
        }
      }
    }


    namespace inf101v22.flappybird {
      namespace Handlers {
        class inf101v22.flappybird.Handlers.FlappyMouseHandler {
            + mouseClicked()
            + mouseEntered()
            + mouseExited()
            + mousePressed()
            + mouseReleased()
        }
      }
    }


    namespace inf101v22.flappybird {
      namespace Handlers {
        class inf101v22.flappybird.Handlers.FlappyObjectHandler {
            {static} + linkedList : LinkedList<FlappyObject>
            {static} + addObject()
            {static} + removeObject()
            {static} + render()
            {static} + tick()
        }
      }
    }


    namespace inf101v22.flappybird {
      namespace Handlers {
        class inf101v22.flappybird.Handlers.FlappyTubeHandler {
            {static} + area : int
            {static} + delay : int
            {static} + groundSize : int
            {static} + maxSize : int
            {static} + minSize : int
            {static} + now : int
            {static} + spacing : int
            {static} - rd : Random
            {static} + spawnTube()
            {static} + tick()
        }
      }
    }


    inf101v22.flappybird.Handlers.FlappyKeyHandler .up.|> java.awt.event.KeyListener
    inf101v22.flappybird.Handlers.FlappyMouseHandler .up.|> java.awt.event.MouseListener

    namespace inf101v22.flappybird {
        namespace Loaders {
          class inf101v22.flappybird.Loaders.GraphicsLoader {
              {static} + loadGraphics()
          }
        }
      }


    namespace inf101v22.flappybird {
      namespace Loaders {
        class inf101v22.flappybird.Loaders.ResourceLoader {
            {static} + load()
        }
      }
    }

    namespace inf101v22.flappybird {
        namespace Mains {
          class inf101v22.flappybird.Mains.FlappyGame {
              {static} + HEIGHT : int
              {static} + WIDTH : int
              {static} + background : BufferedImage
              {static} + flappyBird : FlappyBird
              {static} + gameover : boolean
              {static} + ground : FlappyGround
              {static} + img_gameover : BufferedImage
              + running : boolean
              {static} + score : int
              {static} + startButton : FlappyButton
              ~ thread : Thread
              + init()
              + render()
              + run()
              + start()
              + tick()
          }
        }
      }


      namespace inf101v22.flappybird {
        namespace Mains {
          class inf101v22.flappybird.Mains.FlappyWindow {
              + FlappyWindow()
          }
        }
      }


      namespace inf101v22.flappybird {
        namespace Mains {
          interface inf101v22.flappybird.Mains.GameInterface {
              {abstract} + init()
              {abstract} + render()
              {abstract} + run()
              {abstract} + start()
              {abstract} + tick()
          }
        }
      }


      inf101v22.flappybird.Mains.FlappyGame .up.|> inf101v22.flappybird.Mains.GameInterface
      inf101v22.flappybird.Mains.FlappyGame .up.|> java.lang.Runnable
      inf101v22.flappybird.Mains.FlappyGame -up-|> java.awt.Canvas
      inf101v22.flappybird.Mains.FlappyWindow -up-|> javax.swing.JFrame

      namespace inf101v22.flappybird {
          namespace Song {
            class inf101v22.flappybird.Song.FlappySong {
                {static} - FLAPPYMUSIC : String
                - sequencer : Sequencer
                + doPauseMidiSounds()
                + doStopMidiSounds()
                + doUnpauseMidiSounds()
                + run()
                - doPlayMidi()
                - midiError()
            }
          }
        }


        inf101v22.flappybird.Song.FlappySong .up.|> java.lang.Runnable

      namespace inf101v22.flappybird {
          namespace Supers {
            interface inf101v22.flappybird.Supers.AnimationInterface {
                {abstract} + getCurrentImage()
                {abstract} + getDelay()
                {abstract} + getImages()
                {abstract} + getLoop()
                {abstract} + getObject()
                {abstract} + getX()
                {abstract} + getY()
                {abstract} + isLoop()
                {abstract} + isRunning()
                {abstract} + render()
                {abstract} + setCurrentImage()
                {abstract} + setDelay()
                {abstract} + setImages()
                {abstract} + setObject()
                {abstract} + setRunning()
                {abstract} + setX()
                {abstract} + setY()
                {abstract} + start()
                {abstract} + stop()
                {abstract} + tick()
            }
          }
        }


        namespace inf101v22.flappybird {
          namespace Supers {
            class inf101v22.flappybird.Supers.FlappyAnimation {
                - currentImage : int
                - delay : long
                - images : BufferedImage[]
                - loop : boolean
                - running : boolean
                - startTime : long
                - x : int
                - y : int
                + FlappyAnimation()
                + getCurrentImage()
                + getDelay()
                + getImages()
                + getLoop()
                + getObject()
                + getX()
                + getY()
                + isLoop()
                + isRunning()
                + render()
                + setCurrentImage()
                + setDelay()
                + setImages()
                + setObject()
                + setRunning()
                + setX()
                + setY()
                + start()
                + stop()
                + tick()
            }
          }
        }


        namespace inf101v22.flappybird {
          namespace Supers {
            class inf101v22.flappybird.Supers.FlappyButton {
                + height : int
                {static} + pressed : boolean
                + width : int
                + x : int
                + y : int
                - image : BufferedImage
                + FlappyButton()
                {static} + checkCollision()
                + render()
            }
          }
        }


        namespace inf101v22.flappybird {
          namespace Supers {
            abstract class inf101v22.flappybird.Supers.FlappyObject {
                # height : int
                # velocityX : float
                # velocityY : float
                # width : int
                # x : int
                # y : int
                + FlappyObject()
                + getBounds()
                + getHeight()
                + getVelocityX()
                + getVelocityY()
                + getWidth()
                + getX()
                + getY()
                {abstract} + render()
                + setHeight()
                + setVelocityX()
                + setVelocityY()
                + setWidth()
                + setX()
                + setY()
                {abstract} + tick()
            }
          }
        }


        namespace inf101v22.flappybird {
          namespace Supers {
            interface inf101v22.flappybird.Supers.ObjectInterface {
                {abstract} + getHeight()
                {abstract} + getVelocityX()
                {abstract} + getVelocityY()
                {abstract} + getWidth()
                {abstract} + getX()
                {abstract} + getY()
                {abstract} + setHeight()
                {abstract} + setVelocityX()
                {abstract} + setVelocityY()
                {abstract} + setWidth()
                {abstract} + setX()
                {abstract} + setY()
            }
          }
        }


        inf101v22.flappybird.Supers.FlappyAnimation .up.|> inf101v22.flappybird.Supers.AnimationInterface
        inf101v22.flappybird.Supers.FlappyAnimation o-- inf101v22.flappybird.Supers.FlappyObject : object
        inf101v22.flappybird.Supers.FlappyObject .up.|> inf101v22.flappybird.Supers.ObjectInterface

        namespace inf101v22.flappybird {
            class inf101v22.flappybird.FlappyMain {
                {static} + main()
            }
          }



@enduml